/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

import utfpr.ct.dainf.pratica.PoligonalFechada;
import utfpr.ct.dainf.pratica.PontoXZ;
public class Pratica {

    public static void main(String[] args) {
        PoligonalFechada a = new PoligonalFechada();  
        a.insert(new PontoXZ(-3,2));
        a.insert(new PontoXZ(-3,6));
        a.insert(new PontoXZ(0,2));
        System.out.println("Comprimento da poligonal: "+a.getComprimento(a.get(0),a.get(2) ));
    }
    
}
